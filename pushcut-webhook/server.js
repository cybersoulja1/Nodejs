const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');

const app = express();
const port = process.env.PORT || 3000;

// Middleware to parse JSON bodies
app.use(bodyParser.json());

app.post('/webhook', async (req, res) => {
    const { secret, notificationName, notificationData } = req.body;

    if (!secret || !notificationName) {
        return res.status(400).send({ error: "Missing secret or notificationName" });
    }

    const url = `https://api.pushcut.io/${secret}/notifications/${notificationName}`;

    try {
        const response = await axios.post(url, notificationData, {
            headers: { 'Content-Type': 'application/json' }
        });
        res.status(200).send(response.data);
    } catch (error) {
        if (error.response) {
            res.status(error.response.status).send(error.response.data);
        } else {
            res.status(500).send({ error: "Internal Server Error" });
        }
    }
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});